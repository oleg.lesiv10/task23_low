﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EpamTask23.Controllers
{
    public class MainController : Controller
    {
        // GET: Task

        public ActionResult Index()
        {
            var articles = new[]
            {
                new { Title = "D1", Date = new DateTime(), Text = "T1" },
                new { Title = "D2", Date = new DateTime(), Text = "T2" },
                new { Title = "D3", Date = new DateTime(), Text = "T3" }
            };
            List<ExpandoObject> artExp = new List<ExpandoObject>();
            foreach (var art in articles)
            {
                dynamic artE = new ExpandoObject();
                artE.Title = art.Title;
                artE.Date = art.Date;
                artE.Text = art.Text;
                artExp.Add(artE);
            }

            ViewBag.Artciles = artExp;  
          

            return View(artExp);
        }
    }
}