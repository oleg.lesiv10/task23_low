﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace EpamTask23.Controllers
{
    public class GuestController : Controller
    {
        // GET: Guest
        public static IEnumerable<object> feedbacks = new[] {
        new { Name = " Retartd", Date = new DateTime(), Text = "RRER" },
        new { Name = " Reta3123rtd", Date = new DateTime(), Text = "RRE2222R" },
        new { Name = " Retartd", Date = new DateTime(), Text = "RRER" }
        }.ToList();
        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.Name = "";
            return View(FeedsToExpando(feedbacks));
        }
        [HttpPost]
        public ActionResult Index(FormCollection form)
        {
            feedbacks = feedbacks.Concat( new[] { new { Name = form["Name"], Date = DateTime.Now, Text = form["Text"] } });
            return View(FeedsToExpando(feedbacks));
        }
        public static List<IDictionary<string, Object>> FeedsToExpando (IEnumerable<object> anonList)
        {

            Type type = anonList.First().GetType();
            PropertyInfo[] props = type.GetProperties();

            List<IDictionary<string, Object>> expandos = new List<IDictionary<string, Object>>();

            foreach (var el in anonList)
            {
                var expando = new ExpandoObject() as IDictionary<string, Object>;

                foreach (var prop in props)
                {
                    expando.Add(prop.Name, prop.GetValue(el));
                }
                expandos.Add(expando);
            }
            return expandos;
        }
    }
}